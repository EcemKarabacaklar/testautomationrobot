*** Settings ***

Documentation  Şifre Belirleme Testleri
Library  SeleniumLibrary

#Library  DebugLibrary

#Test Setup  create session
#Test Teardown  close session

*** Variables ***
${LOGIN_PAGE}  https://test-portal.koalay.com/login
${SUPERUSER_PAGE}  https://test-portal.koalay.com/super-user
${button_sifrebelirle}  id=buttonLoginForgotPassword
${acente_username}  69349318924
${acente_password}  Cf123456
${failure_password}  Cf123456..
${button_login}  id=buttonLoginSubmit
${button_devam}  id=buttonForgotSubmit
${button_gönder}  id=buttonForgotSubmit
${button_dogrula}  id=buttonForgotSubmit
${acente_otp}  1234

*** Test Cases ***

Acente Başarılı Login
    create session
    go to login page
    login acente
    close session

Acente Yanlış Şifre

    create session
    go to login page
    username true password failure
    close session

Acente Username ve Password Boş

    create session
    go to login page
    username ve şifre boş
    sleep  5
    close session


Acente Şifre Belirleme
    create session
    go to login page
    şifre belirleme step1
    şifre belirleme step2
    şifre belirleme step3
    şifre belirleme step4


*** Keywords ***

create session
    open browser  about:blank   chrome
    maximize browser window

close session
    close browser

go to login page
    go to  ${LOGIN_PAGE}
login acente
    input text  name=username  ${acente_username}
    input password  name=password  ${acente_password}
    click button  ${button_login}
    sleep  5

username true password failure
    input text  name=username  ${acente_username}
    input password  name=password  ${failure_password}
    click button  ${button_login}
    sleep  5

username ve şifre boş
    sleep  3
    click button  ${button_login}



şifre belirleme step1
    sleep  5
    click button  ${button_sifrebelirle}

şifre belirleme step2
    sleep  5
    input password  name=username  ${acente_username}
    click button  ${button_devam}

şifre belirleme step3
    sleep  5
    click button  ${button_gönder}

şifre belirleme step4
    sleep  5
    input password  name=otp  ${acente_otp}
    click button  ${button_dogrula}

