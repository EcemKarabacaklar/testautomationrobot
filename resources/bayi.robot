*** Settings ***

Documentation  Merchant Referans Link Süreci
Library  SeleniumLibrary

#Library  DebugLibrary

#Test Setup  create session
#Test Teardown  close session

*** Variables ***
${LOGIN_PAGE}  https://test-portal.koalay.com/login
${OPERATION_PAGE}  https://test-portal.koalay.com/operation
${username_Merchant}  9396184025
${password_Merchant}  Cf123456
${login_buton}  id=buttonLoginSubmit
${kasko_surecibaslat_Merchant}  id=buttonProductBoxRouteReference
${dask_surecibaslat_Merchant}  xpath=/html/body/div[1]/div/content/main/div[2]/div/div[2]/div/div[2]/div/div[2]/div[3]/button
${seyahatsaglik_Merchant}  xpath=/html/body/div[1]/div/content/main/div[2]/div/div[2]/div/div[3]/div/div[2]/div[3]/button
${tamamlayicisaglik_surecibaslat_Merchant}  xpath=/html/body/div[1]/div/content/main/div[2]/div/div[2]/div/div[4]/div/div[2]/div[3]/button
${konut_surecibaslat_Merchant}  xpath=/html/body/div[1]/div/content/main/div[2]/div/div[2]/div/div[5]/div/div[2]/div[3]/button
${customer_id}  37978892306
${customer_phone}  5532207819
${KVKK}  xpath=/html/body/div/div[2]/content/main/div[2]/div/div[3]/div/form/div/div[3]/div/div/label/span[1]/span[1]/input
${RefenceSMS}  xpath=/html/body/div/div[2]/content/main/div[2]/div/div[3]/div/form/div/div[4]/div/button


*** Test Cases ***

Merchant Kasko - Trafik Sigortası Süreci Başlat
    create session
    go to login page
    sleep  5
    input username password Merchant
    Click Kasko-trafik Sigortası Süreci Baslat Merchant
    input tckn and phone
    Click KVKK
    Send SMS
    sleep  3

Merchant Dask Sigortası Süreci Başlat


*** Keywords ***

create session
    open browser  about:blank   chrome
    maximize browser window

close session
    close browser

go to login page
    go to  ${LOGIN_PAGE}


input username password Merchant
    input text  name=username  ${username_Merchant}
    input password  name=password  ${password_Merchant}
    click button  ${login_buton}
    sleep  5

input tckn and phone
    sleep  5
    input password  name=tckn  ${customer_id}
    input password  name=phone  ${customer_phone}


Click KVKK
    click element  ${KVKK}

Click Kasko-trafik Sigortası Süreci Baslat Merchant
    sleep  5
    click button  ${kasko_surecibaslat_Merchant}

Click Dask Süreci Başlat InsuranceIntermediary
    sleep  5
    click button  ${dask_surecibaslat_InsuranceIntermediary}

Click Seyahat Sağlık Süreci Başlat InsuranceIntermediary
    sleep  5
    click button  ${seyahatsaglik_surecibaslat_InsuranceIntermediary}

Click Tamamlayıcı Sağlık Sigortası Süreci Başlat InsuranceIntermediary
    sleep  5
    click button  ${tamamlayicisaglik_surecibaslat_InsuranceIntermediary}
Click Konut Sigortası Süreci Başlat InsuranceIntermediary
    sleep  5
    click button  ${konut_surecibaslat_InsuranceIntermediary}

Send SMS
    sleep  5
    click button  ${RefenceSMS}
    sleep  5

Go to homepage
    go to  ${OPERATION_PAGE}
    sleep  5






















