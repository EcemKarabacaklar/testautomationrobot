*** Settings ***

Documentation  My first automation project
Library  SeleniumLibrary

#Library  DebugLibrary

Test Setup  create session
Test Teardown  close session

*** Variables ***
${LOGIN_PAGE}  https://test-portal.koalay.com/login
${OPERATION_PAGE}  https://test-portal.koalay.com/operation
${username_nonageny}  62678139338
${password_nonagency}  Eco123...
${username_agency}  19247240896
${password_agency}  Eco123..
${eposta_nonagency}  Ecem.Karabacaklar@hesapkurdu.com
${verificationCode}  3538
${sendmail_button}  id=buttonReferenceMailSubmit
${login_buton}  id=buttonLoginSubmit
${kasko_surecibaslat_nonagency}  id=buttonProductBoxRouteReference
${kasko_surecibaslat_agency}  id=buttonProductBoxRouteReference
${customer_id}  37978892306
${customer_phone}  5532207819
${KVKK}  xpath=/html/body/div/div[2]/content/main/div[2]/div/div[3]/div/form/div/div[3]/div/div/label/span[1]/span[1]/input
${Refence_1}  id=buttonReferenceSubmit
${Link_Toaster}  xpath=/html/body/div/div[2]/content/main/div[2]/div[2]
${Text_Toaster}  Link gönderildi.


*** Test Cases ***


Acente Kasko - Trafik Süreci Başlat
    go to login page
    input username password acente
    Click Kasko-trafik Süreci Baslat Acente
    input tckn and phone
    Click KVKK
    Send SMS
    sleep  5


Bayi Kasko - Trafik Süreci Başlat
    go to login page
    input username password bayi
    Click Kasko-trafik Süreci Baslat Bayi
    input tckn and phone
    Click KVKK
    Send SMS
    input email and otp
    Click Send Mail
    sleep  5



*** Keywords ***

create session
    open browser  about:blank   chrome
    maximize browser window


close session
    close browser

go to login page
    go to  ${LOGIN_PAGE}

input username password bayi
    input text  name=username  ${username_nonageny}
    input password  name=password  ${password_nonagency}
    click button  ${login_buton}
    sleep  5


input username password acente
    input text  name=username  ${username_agency}
    input password  name=password  ${password_agency}
    click button  ${login_buton}
    sleep  5


Click Kasko-trafik Süreci Baslat Bayi
    sleep  5
    click button  ${kasko_surecibaslat_nonagency}

Click Kasko-trafik Süreci Baslat Acente
    sleep  5
    click button  ${kasko_surecibaslat_agency}

Check Toaster
    double click element  id=snackbarSnackBarBoxLabAlert

Click Dask
    sleep  5
    click button  ${dask_surecibaslat}

Click Seyahat Sağlık
    sleep  5
    click button  ${seyahat_surecibaslat}

input tckn and phone
    sleep  5
    input password  id=inputReferenceTckn  ${customer_id}
    input password  id=inputReferencePhone  ${customer_phone}



Click KVKK
    click element  ${KVKK}

Send SMS
    sleep  5
    click button  ${Refence_1}
    sleep  5


input email and otp
    input text  id=inputReferenceMail  ${eposta_nonagency}
    input password  id=inputReferenceVerificationCode  ${verificationCode}
    sleep  5

Click Send Mail
    sleep  3
    click button  ${sendmail_button}






















