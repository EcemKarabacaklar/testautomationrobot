*** Settings ***
Documentation  My first robot test
Library  SeleniumLibrary
Library  DebugLibrary

Test Setup  create session
Test Teardown  close session

*** Variables ***
${HOMEPAGE}  https://test-portal.koalay.com/login
${HELP_PAGE}  ${HOMEPAGE}

*** Test Cases ***
help page tests
    go to help page
    title should be Yardım
    debug

*** Keywords ***
create session
    open browser  about:blank   chrome
    maximize browser window

go to help page
    go to  ${HELP_PAGE}




