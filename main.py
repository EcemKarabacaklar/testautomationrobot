from selenium import webdriver
import time
import click


driver ="C:/Users/KA018/Desktop/Selenium/drivers/chromedriver.exe"
browser = webdriver.Chrome(driver)


# siteye bağlanmak
browser.get("https://test-portal.koalay.com/login");

# pencereyi tam ekran yapmak
browser.maximize_window()
time.sleep(2)

# Bayi - login olmak
UserName =  browser.find_element_by_name("username")
UserName.send_keys("53246981930")

Password = browser.find_element_by_name("password")
Password.send_keys("Eco123..")

Login = browser.find_element_by_xpath("/html/body/div/div/div[2]/div/form/input")
Login.click()
print("Non Agency - Successful Login.")
time.sleep(5)

# Trafik/Kasko Sigortası Referans akıs başlatmak
SurecBaslat = browser.find_element_by_xpath("/html/body/div[1]/div/content/main/div[2]/div/div[3]/div/div[1]/div/div[2]/div[3]/button")
SurecBaslat.click()
print("Trafik/Kasko süreç başlatıldı.")
time.sleep(5)

TCKN = browser.find_element_by_name("tckn")
TCKN.send_keys("37978892306")

Phone = browser.find_element_by_name("phone")
Phone.send_keys("5464341993")

KVKK = browser.find_element_by_xpath("/html/body/div/div[2]/content/main/div[2]/div/div[3]/div/form/div/div[3]/div/div/label/span[1]/span[1]/input")
KVKK.click()
time.sleep(5)

# SMS Gönder
SendSMS = browser.find_element_by_xpath("/html/body/div/div[2]/content/main/div[2]/div/div[3]/div/form/div/div[4]/div/button")
SendSMS.click()
print("Trafik/Kasko teklifleri almak için SMS gönderildi.")

